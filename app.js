const io = require('socket.io-client')

const VEX_WEBSOCKET = 'https://ws.vexsoft.com.br:3005'
 

const socket = io(VEX_WEBSOCKET, {
    path: '/socket.io',
    secure: true,
    rejectUnauthorized: false
});

const empresa = {
    token: 'LOC786'
}
socket.emit('token', empresa)

socket.on('novaVistoriaRealizadaToken', function(data) {
    const vistoriaId = data.id
    console.log(vistoriaId)
})